#
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "field:product.product,expiration_state:"
msgid "Expiration State"
msgstr "Ablaufstatus"

msgctxt "field:product.product,expiration_time:"
msgid "Expiration Time"
msgstr "Ablaufzeit"

msgctxt "field:product.product,shelf_life_state:"
msgid "Shelf Life Time State"
msgstr "Lagerzeitstatus"

msgctxt "field:product.product,shelf_life_time:"
msgid "Shelf Life Time"
msgstr "Lagerzeit"

msgctxt "field:product.template,expiration_state:"
msgid "Expiration State"
msgstr "Ablaufstatus"

msgctxt "field:product.template,expiration_time:"
msgid "Expiration Time"
msgstr "Ablaufzeit"

msgctxt "field:product.template,shelf_life_state:"
msgid "Shelf Life Time State"
msgstr "Lagerzeitstatus"

msgctxt "field:product.template,shelf_life_time:"
msgid "Shelf Life Time"
msgstr "Lagerzeit"

msgctxt "field:stock.configuration,shelf_life_delay:"
msgid "Shelf Life Delay"
msgstr "Lagerzeit Wartedauer"

msgctxt "field:stock.location.lot.shelf_life,create_date:"
msgid "Create Date"
msgstr "Erstellungsdatum"

msgctxt "field:stock.location.lot.shelf_life,create_uid:"
msgid "Create User"
msgstr "Erstellt durch"

msgctxt "field:stock.location.lot.shelf_life,id:"
msgid "ID"
msgstr "ID"

msgctxt "field:stock.location.lot.shelf_life,rec_name:"
msgid "Record Name"
msgstr "Bezeichnung des Datensatzes"

msgctxt "field:stock.location.lot.shelf_life,shelf_life_delay:"
msgid "Shelf Life Delay"
msgstr "Lagerzeit Wartedauer"

msgctxt "field:stock.location.lot.shelf_life,write_date:"
msgid "Write Date"
msgstr "Zuletzt geändert"

msgctxt "field:stock.location.lot.shelf_life,write_uid:"
msgid "Write User"
msgstr "Letzte Änderung durch"

msgctxt "field:stock.lot,expiration_date:"
msgid "Expiration Date"
msgstr "Ablaufdatum"

msgctxt "field:stock.lot,expiration_state:"
msgid "Expiration State"
msgstr "Ablaufstatus"

msgctxt "field:stock.lot,shelf_life_expiration_date:"
msgid "Shelf Life Expiration Date"
msgstr "Lagerzeit Ablaufdatum"

msgctxt "field:stock.lot,shelf_life_expiration_state:"
msgid "Shelf Life Expiration State"
msgstr "Lagerzeit Ablaufdatum"

msgctxt "help:product.product,expiration_time:"
msgid "In number of days"
msgstr "In Anzahl von Tagen"

msgctxt "help:product.product,shelf_life_time:"
msgid "In number of days"
msgstr "In Anzahl von Tagen"

msgctxt "help:product.template,expiration_time:"
msgid "In number of days"
msgstr "In Anzahl von Tagen"

msgctxt "help:product.template,shelf_life_time:"
msgid "In number of days"
msgstr "In Anzahl von Tagen"

msgctxt "help:stock.configuration,shelf_life_delay:"
msgid "The delay before removal from the forecast."
msgstr "Die Zeitspanne vor dem entfernen aus der Bedarfsermittlung."

msgctxt "help:stock.location.lot.shelf_life,shelf_life_delay:"
msgid "The delay before removal from the forecast."
msgstr "Die Zeitspanne vor dem entfernen aus der Bedarfsermittlung."

msgctxt "model:ir.message,text:msg_lot_modify_expiration_date_period_close"
msgid ""
"You cannot modify expiration dates of lot \"%(lot)s because it is used on "
"move \"%(move)s\" in a closed period."
msgstr ""
"Das Ablaufdatum von Charge \"%(lot)s\" kann nicht verändert werden, weil sie"
" in Lagerbewegung \"%(move)s\" in einer geschlossenen Lagerperiode verwendet"
" wird."

msgctxt "model:ir.message,text:msg_move_lot_expired"
msgid "You cannot process move \"%(move)s\" because its lot \"%(lot)s\" has expired."
msgstr ""
"Die Lagerbewegung \"%(move)s\" kann nicht ausgeführt werden, weil die "
"zugewiesene Charge \"%(lot)s\" abgelaufen ist."

msgctxt "model:ir.message,text:msg_period_close_sled"
msgid ""
"You cannot close periods before the expiration of lot \"%(lot)s\" "
"(%(date)s)."
msgstr ""
"Lagerperioden können nicht vor Ablauf der Charge \"%(lot)s\" (%(date)s) "
"geschlossen werden."

msgctxt "model:res.group,name:group_stock_force_expiration"
msgid "Stock Force Expiration"
msgstr "Lager Ablauferzwingung"

msgctxt "model:stock.location.lot.shelf_life,name:"
msgid "Stock Configuration Lot Shelf Life"
msgstr "Einstellungen Lager Lagerzeit"

msgctxt "selection:product.product,expiration_state:"
msgid "None"
msgstr "Keiner"

msgctxt "selection:product.product,expiration_state:"
msgid "Optional"
msgstr "Optional"

msgctxt "selection:product.product,expiration_state:"
msgid "Required"
msgstr "Erforderlich"

msgctxt "selection:product.product,shelf_life_state:"
msgid "None"
msgstr "Keiner"

msgctxt "selection:product.product,shelf_life_state:"
msgid "Optional"
msgstr "Optional"

msgctxt "selection:product.product,shelf_life_state:"
msgid "Required"
msgstr "Erforderlich"

msgctxt "selection:product.template,expiration_state:"
msgid "None"
msgstr "Keiner"

msgctxt "selection:product.template,expiration_state:"
msgid "Optional"
msgstr "Optional"

msgctxt "selection:product.template,expiration_state:"
msgid "Required"
msgstr "Erforderlich"

msgctxt "selection:product.template,shelf_life_state:"
msgid "None"
msgstr "Keiner"

msgctxt "selection:product.template,shelf_life_state:"
msgid "Optional"
msgstr "Optional"

msgctxt "selection:product.template,shelf_life_state:"
msgid "Required"
msgstr "Erforderlich"

msgctxt "selection:stock.lot,expiration_state:"
msgid "None"
msgstr "Keiner"

msgctxt "selection:stock.lot,expiration_state:"
msgid "Optional"
msgstr "Optional"

msgctxt "selection:stock.lot,expiration_state:"
msgid "Required"
msgstr "Erforderlich"

msgctxt "selection:stock.lot,shelf_life_expiration_state:"
msgid "None"
msgstr "Keiner"

msgctxt "selection:stock.lot,shelf_life_expiration_state:"
msgid "Optional"
msgstr "Optional"

msgctxt "selection:stock.lot,shelf_life_expiration_state:"
msgid "Required"
msgstr "Erforderlich"

msgctxt "view:product.template:"
msgid "Shelf Life"
msgstr "Lagerzeit"
